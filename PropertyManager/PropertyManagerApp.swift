//
//  PropertyManagerApp.swift
//  PropertyManager
//
//  Created by Camilo Cabana on 2022/02/10.
//

import SwiftUI

@main
struct PropertyManagerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
